import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';

export default class ChatAccesoryView extends React.Component {

  render() {
    return (
    <View>
      <View style={styles.row}>
        <View style={styles.columm}>
        <Text>A1</Text>
        </View>
        <View style={styles.columm}>
        <Text>A2</Text>
        </View>
        <View style={styles.columm}>
        <Text>A3</Text>
        </View>
        <View style={styles.columm}>
        <Text>A4</Text>
        </View>
      </View>
      <View style={styles.row}>
        <View style={styles.columm}>
        <Text>A5</Text>
        </View>
        <View style={styles.columm}>
        <Text>A6</Text>
        </View>
        <View style={styles.columm}>
        <Text>A7</Text>
        </View>
        <View style={styles.columm}>
        <Text>A8</Text>
        </View>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  columm: {
    flex: 1,
    alignItems: 'center',
  },
});
