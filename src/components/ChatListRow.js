import React from 'react';
import {Platform, StyleSheet, FlatList, View, Alert, TouchableOpacity} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import { observer } from 'mobx-react';
import ChatStore from '../store/ChatStore';
import { Badge, Text } from 'native-base';

@observer
export default class ChatListRow extends React.Component {
  render() {
    const item = this.props.item;
    return (
      <TouchableOpacity
        style={{height: 70, flexDirection: 'row'}}
        onPress={() => this.props.onPress(item)}
        onLongPress={() => this.props.onLongPress(item)}>
        <Grid>
          <Col style={{ width: 50, justifyContent: 'center', alignItems: 'center'}}>
          <Text>{item.roomkey}</Text>
          </Col>
          <Col style={{ paddingLeft:5, paddingRight:5}}>
            <Row style={{ height: 30, alignItems: 'center'}}>
                <Text style={{fontSize: 14, color: Colors.blackColor}}>{item.party.toString()}</Text>
            </Row>
            <Row>
                <Text style={{fontSize: 12, color: Colors.grayColor}}>{item.content.get('text')}</Text>
            </Row>
          </Col>
          <Col style={{ width: 80}}>
            <Row style={{ height: 40, justifyContent: 'center'}}>
                <Text style={{fontSize: 10, color: Colors.grayColor}}
                  numberOfLines={2}
                  textAlign='right'>{item.date.toString()}</Text>
            </Row>
            <Row style={{ justifyContent: 'center', alignItems: 'center'}}>
              <Badge fontSize={11}>
                <Text>{item.unread}</Text>
              </Badge>
            </Row>
          </Col>
        </Grid>

      </TouchableOpacity>
    );
  }
}