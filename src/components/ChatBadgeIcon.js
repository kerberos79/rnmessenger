import React from 'react';
import {
  Text,
  View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconBadge from 'react-native-icon-badge';
import Colors from '../constants/Colors';
import { observer } from 'mobx-react';
import Mobx from 'mobx';
import ChatStore from '../store/ChatStore';

@observer
export default class ChatBadgeIcon extends React.Component {
  render() {
    var unread = ChatStore.totalUnread;
    return (
      <IconBadge
        MainElement={
          <View style={{width:60, height:30, flexDirection: 'row',alignItems: 'center',justifyContent: 'center'}}>
          <Icon
            name={this.props.name}
            size={26}
            style={{ marginBottom: -3 }}
            color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
            />
          </View>
        }
        BadgeElement={
          <Text style={{fontSize:12, color:'#FFFFFF'}}>{unread}</Text>
        }
        IconBadgeStyle={{
            position:'absolute',
            top:1,
            right:1,
            minWidth:18,
            height:18,
            backgroundColor: Colors.redColor}}
        Hidden={unread===0}
      />
    );
  }
}