import React from 'react';
import {Platform, StyleSheet, View, Text, Alert, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';

export default class IconButton extends React.Component {
  render() {
    return (
			<TouchableOpacity
        onPress={this.props.handlePressAction}
        style={{width:this.props.width, height:this.props.height, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Icon
          name={this.props.name}
          size={this.props.size}
          color={this.props.color}
        />
      </TouchableOpacity>
    );
  }
}