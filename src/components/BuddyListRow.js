import React from 'react';
import {Platform, StyleSheet, FlatList, View, Alert, TouchableOpacity} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import { observer } from 'mobx-react';
import UserStore from '../store/UserStore';
import { Badge, Text } from 'native-base';

@observer
export default class BuddyListRow extends React.Component {
  render() {
    const item = this.props.item;
    return (
      <TouchableOpacity
        style={{height: 70, flexDirection: 'row'}}
        onPress={() => this.props.onPress(item)}
        onLongPress={() => this.props.onLongPress(item)}>
        <Grid>
          <Col style={{ width: 50, justifyContent: 'center', alignItems: 'center'}}>
          <Text>{item.avt}</Text>
          </Col>
          <Col style={{ paddingLeft:5, paddingRight:5}}>
            <Row style={{ height: 30, alignItems: 'center'}}>
                <Text style={{fontSize: 14, color: Colors.blackColor}}>{item.name}</Text>
            </Row>
            <Row>
                <Text style={{fontSize: 12, color: Colors.grayColor}}>{item.age}</Text>
            </Row>
          </Col>
        </Grid>

      </TouchableOpacity>
    );
  }
}