import React from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import ChatBadgeIcon from '../components/ChatBadgeIcon';
import HomeScreen from '../screens/HomeScreen';
import BuddyScreen from '../screens/BuddyScreen';
import ChatRoomScreen from '../screens/ChatRoomScreen';
import ChatScreen from '../screens/ChatScreen';
import MoreScreen from '../screens/MoreScreen';
import ProfileScreen from '../screens/ProfileScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='ios-person'
    />
  ),
};

const BuddyStack = createStackNavigator({
  Buddy: BuddyScreen,
});

BuddyStack.navigationOptions = {
  tabBarLabel: 'Buddy',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='ios-heart'
    />
  ),
};

const ChatRoomStack = createStackNavigator({
  ChatRoom: ChatRoomScreen,
});

ChatRoomStack.navigationOptions = {
  tabBarLabel: 'Chat',
  tabBarIcon: ({ focused }) => (
    <ChatBadgeIcon
      focused={focused}
      name='ios-text'
    />
  ),
};

const MoreStack = createStackNavigator({
  More: MoreScreen,
});

MoreStack.navigationOptions = {
  tabBarLabel: 'More',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='ios-more'
    />
  ),
};

const MainTabNavigator = createBottomTabNavigator({
  HomeStack,
  BuddyStack,
  ChatRoomStack,
  MoreStack,
});

MainTabNavigator.navigationOptions =  {
  header: null
}
export default createStackNavigator({
  Main: MainTabNavigator,
  Chat: ChatScreen,
  Profile:ProfileScreen,
});