const tintColor = '#2f95dc';

export default {
  tintColor,
  blackColor:'#000',
  whiteColor:'#fff',
  grayColor:'#BDBDBD',
  redColor:'red',
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  seperator: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  naviIcon: '#fff',
  naviTitle: '#fff',
  naviBackground: '#0D47A1',
  registerButtonColor: '#03A9F4', //md_light_blue_500
  maleButtonColor: '#03A9F4', //md_light_blue_500
  femaleButtonColor: '#E57373', //md_red_300
};
