import { types, destroy, unprotect } from "mobx-state-tree"

const TestStore = types.model({
  })
  .views(self => ({
  })
  )
  .actions(self => ({
    connectHttp2() {
      fetch('https://update.ucwaremobile.com/index.html', {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          'Accept-Encoding': 'gzip'
        },
        //body: "firstName=Nikhil&favColor=blue&password=easytoguess"
      })
      .then((response) =>  {
        console.error('connectHttp2 result:' + response);
      })
      .catch((error) => {
        console.error('connectHttp2 Error:'+error);
      });
    },
  }))
  .create({
  })

export default TestStore