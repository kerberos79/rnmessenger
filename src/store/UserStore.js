import { types, destroy, unprotect } from "mobx-state-tree"
import { realm } from "./Database"

const UserInfo = types.model('UserInfo', {
  uid: types.string,
  name:types.string,
  sub:types.string,
  age:types.number,
  sex:types.string,
  avt:types.string,
  lev:types.number,
  warn:types.number,
  lat: types.number,
  long: types.number,
}).actions(self => ({
}))


const UserStore = types.model({
  buddys:types.array(UserInfo),
  userInfos:types.array(UserInfo),
  })
  .views(self => ({
  })
  )
  .actions(self => ({
    addUser(userInfo) {
      self.userInfos.push(userInfo)
    },
    removeBuddy(userInfo) {
      self.userInfos.remove(userInfo)
    },
    addBuddy(buddy) {
      self.buddys.push(buddy)
    },
    removeBuddy(buddy) {
      self.userInfos.remove(buddy)
    },
    loadUserInfo() {
      let users = realm.objects('UserInfo');
      if(users.length>0) {
        for(let i=0;i<users.length;i++) {
          let userInfo = UserInfo.create({
            uid: users[i].uid,
            name:users[i].name,
            sub:users[i].sub,
            age:users[i].age,
            sex:users[i].sex,
            avt:users[i].avt,
            lev:users[i].lev,
            warn:users[i].warn,
            lat: users[i].lat,
            long: users[i].long,
          });
          self.addUser(userInfo);
        }
      }
      else {
        console.log('==loadUserInfo is lenth 0.');
      }
    },
    loadBuddy() {
      let buddys = realm.objects('BuddyList');
      if(buddys.length>0) {
        for(let i=0;i<buddys.length;i++) {
          let buddyId = buddys[i].uid;
          const userInfo = self.userInfos.find(u => u.uid === buddyId);
          if(userInfo!=null) {
            self.addBuddy(userInfo);
          }
        }
      }
      else {
        console.log('==loadBuddys is lenth 0.');
      }

    },
    createNewUser(uid, name, subject, age, sex) {
      const newUser = UserInfo.create({
        uid: uid,
        name:name,
        sub:subject,
        age:age,
        sex:sex,
        avt:'',
        lev:0,
        warn:0,
        lat: 0.,
        long: 0.,
      });
      self.addUser(newUser);
      self.storeUserInfo(newUser)
    },
    addNewBuddy(buddyId) {
      const userInfo = self.userInfos.find(u => u.uid === buddyId);
      if(userInfo!=null) {
        const existBuddy = self.buddys.find(u => u.uid === buddyId);
        if(existBuddy!=null) {
          console.log('existBuddy : '+buddyId);
        }
        else{
          console.log('addNewBuddy : '+buddyId);
          self.addBuddy(userInfo);
          self.storeBuddy(buddyId);
        }
      }
    },
    storeUserInfo(userInfo) {
      try {
        realm.write(() => {
          realm.create('UserInfo',  { 
            uid: userInfo.uid,
            name:userInfo.name,
            sub:userInfo.sub,
            age:userInfo.age,
            sex:userInfo.sex,
            avt:userInfo.avt,
            lev:userInfo.lev,
            warn:userInfo.warn,
            lat: userInfo.lat,
            long: userInfo.long});
        });
      } catch (e) {
        console.log("Error : "+e);
      }
    },
    storeBuddy(userId) {
      try {
        realm.write(() => {
          realm.create('BuddyList',  { 
            uid: userId});
        });
      } catch (e) {
        console.log("Error : "+e);
      }
    }
  }))
  .create({
  })

export default UserStore