import { types, destroy, unprotect } from "mobx-state-tree"
import { realm } from "./Database"


const SystemStore = types.model({
  uid:types.string,
  updatedUser:types.number,
  })
  .views(self => ({
  })
  )
  .actions(self => ({
    loadFromDB() {
      
    },
  }))
  .create({
  })

export default SystemStore