const Realm = require('realm');

const SystemScheme = {
  name: 'System', 
  primaryKey: 'uid',
  properties: {
    uid: {type : 'string', indexed: true },
    updatedUsed:{type : 'date'},
  }
};

const BuddyListScheme = {
  name: 'BuddyList', 
  primaryKey: 'uid',
  properties: {
    uid: {type : 'string', indexed: true },
  }
};

const UserInfoScheme = {
  name: 'UserInfo', 
  primaryKey: 'uid',
  properties: {
    uid: {type : 'string', indexed: true },
    name: {type : 'string'},
    sub: {type : 'string'},
    age: {type: 'int', default: 0},
    sex: {type: 'string'}, // 'o':one, 'm':multiple, 'n':nitification
    avt: {type: 'string'},
    lev: {type: 'int', default: 0},
    warn: {type: 'int', default: 0},
    lat: {type: 'float', default: 0.},
    long: {type: 'float', default: 0.},
  }
};

const ChatRoomScheme = {
  name: 'ChatRoom', 
  primaryKey: 'roomkey',
  properties: {
    roomkey: {type : 'string', indexed: true },
    party:{type: 'list', objectType: 'string'},
    type: {type: 'string'}, // 'o':one, 'm':multiple, 'n':nitification
    content: {type: 'string'},
    date: {type: 'date'},
    unread: {type: 'int', default: 0},
  }
};

const ChatScheme = {
  name: 'Chat', 
  primaryKey: 'chatkey',
  properties: {
    roomkey: {type : 'string', indexed: true },
    chatkey: {type : 'string', indexed: true }, // roomkey+timestamp+sendId
    senderId: {type: 'string'},
    party:{type: 'string[]'},
    unreadId: {type: 'string[]'},
    content: {type: 'string'},
    date: {type: 'date'},
  }
};
const config = {
  schema: [SystemScheme, BuddyListScheme, UserInfoScheme, ChatRoomScheme, ChatScheme],
};

export const realm = new Realm(config);