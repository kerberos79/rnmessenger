import { types, destroy, unprotect } from "mobx-state-tree"
import { realm } from "./Database"

const ChatRoom = types.model('ChatRoom', {
  roomkey: types.string,
  type:types.string,
  party:types.array(types.string),
  content: types.map(types.string),
  date: types.Date,
  unread:types.number,
}).actions(self => ({
  increaseUnread() {
    return ++self.unread;
  },
}))


const ChatStore = types.model({
  loginUid: types.string,
  rooms: types.array(ChatRoom),
  totalUnread:types.number,
  currentChatRoomkey:types.string,
  })
  .views(self => ({
  })
  )
  .actions(self => ({
    addRoom(room) {
      self.rooms.push(room)
    },
    removeRoom(room) {
      destroy(room)
    },
    updateTotalUnread() {
      let totalUnread = 0;
      for(var i=0;i<self.rooms.length;i++)
        totalUnread+=self.rooms[i].unread;
      self.totalUnread=totalUnread;
    },
    loadRooms() {
      let rooms = realm.objects('ChatRoom').sorted('date', true);
      if(rooms.length>0) {
        for(let i=0;i<rooms.length;i++) {
          console.log('### '+rooms[i].party);
          let room = ChatRoom.create({
            roomkey: rooms[i].roomkey,
            type:rooms[i].type,
            date: rooms[i].date,
            unread: rooms[i].unread,
          });
          unprotect(room);

          let party = rooms[i].party.toString().split(',')
          for( var j=0;j<party.length;j++)
            room.party.push(party[j]);
            
          let chatContent = JSON.parse(rooms[i].content);
          for (var key in chatContent){
              var attrName = key;
              var attrValue = chatContent[key];
              room.content.set(attrName, attrValue);
          }
          console.log('## loadRoom mobx ['+room.roomkey+']'+JSON.stringify(room.content.toJSON()));
          self.addRoom(room);    
        }
        self.updateTotalUnread();
      }
      else {
        console.log('==loadRooms is lenth 0.');
      }

    },
    createRoom(userId, content, roomtype) {
      //let date = new Date().format("yyyyMMddHHmmsssss");
      const date = new Date().getTime().toString();
      const room = ChatRoom.create({
        roomkey: date,
        type:roomtype,
        party: [self.loginUid, userId],
        date: new Date(),
        unread: 1,
      });
      unprotect(room);
      room.content.set('type', 'text');
      room.content.set('text', '테스트입니다.'+self.rooms.length);
      self.addRoom(room);   
      self.updateTotalUnread();
      try {
        realm.write(() => {
          realm.create('ChatRoom', 
                        { roomkey: room.roomkey, 
                          type: room.type, 
                          party: room.party, 
                          content: JSON.stringify(room.content.toJSON()), 
                          date: room.date, 
                          unread: room.unread });
        });
      } catch (e) {
        console.log("Error : "+e);
      }
    },
    deleteRoom(room) {
      const roomkey = room.roomkey;
      try {
        realm.write(() => {
          let deleteRooms = realm.objects('ChatRoom').filtered('roomkey = "'+roomkey+'"');
          console.log('##deleteRoom['+roomkey+' : '+deleteRooms.length);
          realm.delete(deleteRooms);
        });
      } catch (e) {
        console.log("deleteRoom Error : "+e);
      }
      self.removeRoom(room);
      self.updateTotalUnread();
    },
    updateRoom(room) {
      const roomkey = room.roomkey;
      let unreadCount = 0;
      let updateRooms = self.rooms.filter(room => room.roomkey === roomkey);
      for (let updateRoom of updateRooms)
        unreadCount = updateRoom.increaseUnread();
      self.updateTotalUnread();
      try {
        realm.write(() => {
          let updateRooms = realm.objects('ChatRoom').filtered('roomkey = "'+roomkey+'"');
          console.log('##updateRoom['+roomkey+' : '+updateRooms.length);
          for (var i=0; i<updateRooms.length;i++) {
            updateRooms[i].unread= unreadCount;
          }
        });
      } catch (e) {
        console.log("updateRoom Error : "+e);
      }
    },
    createGiftChatInfoFromRealmObject(chatInfo) {
      const chatContent = JSON.parse(chatInfo.content);
      if(chatContent.type === 'text') {
        return {
          _id: chatInfo.chatkey,
          text: chatContent.data,
          createdAt: new Date(chatInfo.date),
          user: {
            _id: chatInfo.senderId,
            name: chatInfo.senderId,
            avatar: 'https://placeimg.com/140/140/any',
          }
        }
      }
    },
    loadChat(messages, roomkey) {
      let chats = realm.objects('Chat').filtered('roomkey = "'+roomkey+'"').sorted('date', true);
      let index = 0;
      if(chats.length>0) {
        for(let i=0;i<chats.length;i++) {
          messages.push(self.createGiftChatInfoFromRealmObject(chats[i]));
        }
      }
      else {
        console.log('==loadChat is lenth 0.');
      }
      self.currentChatRoomkey = roomkey;
    },
    sendMessage(roomkey, senderId, destIdArray, content) {
      const date = new Date();
      const timestamp = date.getTime().toString();
      console.log('timestamp :'+timestamp);
      const chatInfo = {
        roomkey:roomkey, 
        chatkey:senderId+timestamp,
        senderId:senderId,
        party:destIdArray,
        unreadId:destIdArray,
        content:content,
        date:date
      }
      chatInfo.party.push(senderId);
      self.storeChatInfo(chatInfo);
    },
    onIncomingMessage(chatInfo) {
      self.storeChatInfo(chatInfo);
      if(self.currentChatRoomkey===chatInfo.roomkey) {

      }

    return {
      _id: chatInfo.chatkey,
      text: content.data,
      createdAt: new Date(chatInfo.date),
      user: {
        _id: chatInfo.senderId,
        name: chatInfo.senderId,
        avatar: 'https://placeimg.com/140/140/any',
      }
    }
    },
    storeChatInfo(chatInfo) {
      try {
        realm.write(() => {
          realm.create('Chat',  { roomkey: chatInfo.roomkey, 
                                  chatkey: chatInfo.chatkey, 
                                  senderId: chatInfo.senderId, 
                                  party: chatInfo.party, 
                                  unreadId: chatInfo.unreadId,
                                  content: JSON.stringify(chatInfo.content), 
                                  date: chatInfo.date});
        });
      } catch (e) {
        console.log("Error : "+e);
      }
    }
  }))
  .create({
    loginUid:'kang',
    rooms: [],
    totalUnread:0,
    currentChatRoomkey:'',
  })

export default ChatStore