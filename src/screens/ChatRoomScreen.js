import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  FlatList,
  View} from 'react-native';
import ActionSheet from 'react-native-actionsheet'
import { observer } from 'mobx-react';
import Mobx from 'mobx';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import ChatStore from '../store/ChatStore';
import ChatListRow from '../components/ChatListRow';
import IconButton from '../components/IconButton';

var ACTIONSEET_BUTTONS = [ "Delete", "Cancel"];

@observer
export default class ChatRoomScreen extends React.Component {
  static navigationOptions =  ({navigation, screenProps})=>({
    title: 'ChatRoom',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerRight: (
			<IconButton
        handlePressAction={navigation.getParam('addRoom')}
        name='ios-add'
        size={26}
        width={40}
        height={40}
        color={Colors.whiteColor}/>
    ),
  });

  constructor(props) {
    super(props);
    this.state = { 
      clicked:false,
      selectedItem:null,
    };
  }
  componentDidMount(){
    this.props.navigation.setParams({ addRoom: this.addRoom });
  }
  addRoom =  (key) => {
    console.log('addRoom');
    ChatStore.createRoom('test', '테스트입니다.', 'o');
  };
  onPressItem =  (item) => {
    this.props.navigation.push('Chat', {roomkey:item.roomkey});
  };

  onLongPressItem =  (item) => {
    this.setState({
      selectedItem:item
    });
    this.ActionSheet.show();
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: Colors.seperator,
        }}
      />
    );
  }

  renderItem = ({item}) => (
    <ChatListRow
      item={item}
      onPress={() => this.onPressItem(item)}
      onLongPress={() => this.onLongPressItem(item)}/>
  );
  handleActionSheet = (index) => {
    if(index ==0) {
      ChatStore.deleteRoom(this.state.selectedItem);
    }
    else if(index ==1) {

    }
    this.setState({
      selectedItem:null
    });
  };

  render() {
    const dataSource = ChatStore.rooms.slice();
    return (
      <View style={styles.container}>
        <FlatList
          data={dataSource}
          keyExtractor={(item, index) => item.roomkey}
          ItemSeparatorComponent = {this.FlatListItemSeparator}
          renderItem={this.renderItem}
        />
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={ACTIONSEET_BUTTONS}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={this.handleActionSheet}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
