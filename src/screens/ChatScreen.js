import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  Platform,
  Dimensions,
  Keyboard,
  LayoutAnimation,
  UIManager,
  View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import { GiftedChat } from 'react-native-gifted-chat'
import emojiUtils from 'emoji-utils';
import SlackMessage from '../components/SlackMessage';
import ChatAccesoryView from '../components/ChatAccesoryView';
import IconButton from '../components/IconButton';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import ChatStore from '../store/ChatStore';

export default class ChatScreen extends React.Component {
  static navigationOptions =  ({navigation, screenProps})=>({
    title: 'Chat',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerRight: (
			<IconButton
        handlePressAction={navigation.getParam('newChat')}
        name='ios-add'
        size={26}
        width={40}
        height={40}
        color={Colors.whiteColor}/>
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      accesoryHeight:0,
      keyboardOpen:false,
      roomkey:this.props.navigation.getParam('roomkey', ''),
      text:'',
      index: 0,
    };
    this.hideAccesory = this.hideAccesory.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this)
  }

  componentDidMount(){
    this.props.navigation.setParams({ newChat: this.newChat });
  }
  componentWillMount() {

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.hideAccesory);
    
  }
  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
  }
  onLoadEarlier =()=> {
    console.log('onLoadEarlier');
    this.setState((previousState) => {
      return { isLoadingEarlier: true }
    })
    let initMessage = [];
    ChatStore.loadChat(initMessage, this.state.roomkey);
    this.setState({
      messages:initMessage
    })
  }
  newChat =  (key) => {
    console.log('newChat');
    const content={ type:'text',
                    data:'newChat' };
    const date = new Date();
    const senderId = 'lee';
    const destIdArray = ['kang'];
    const timestamp = date.getTime().toString();
    const chatInfo = {
      roomkey:this.state.roomkey, 
      chatkey:this.state.roomkey+senderId+timestamp,
      senderId:senderId,
      party:destIdArray,
      unreadId:destIdArray,
      content:content,
      date:date
    }
    chatInfo.party.push(senderId);
    const incomingMessage = ChatStore.onIncomingMessage(chatInfo);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, incomingMessage)
    }));
  };
  onSend(messages = []) {

    const content={ type:'text',
                    data:this.state.text }
    ChatStore.sendMessage(this.state.roomkey, ChatStore.loginUid, ['lee'], content);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  renderMessage(props) {
    const { currentMessage: { text: currText } } = props;

    let messageTextStyle;

    // Make "pure emoji" messages much bigger than plain text.
    if (currText && emojiUtils.isPureEmojiString(currText)) {
      messageTextStyle = {
        fontSize: 28,
        // Emoji get clipped if lineHeight isn't increased; make it consistent across platforms.
        lineHeight: Platform.OS === 'android' ? 34 : 30,
      };
    }

    return (
      <SlackMessage {...props} messageTextStyle={messageTextStyle} />
    );
  }
  handleAvatarPress = () => {
  }
  handleAudio = () => {
  }
  handleAddAction = () => {
    console.log('handleAddAction '+this.state.index);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    
    if(this.state.accesoryHeight===0) {
      this.setState({accesoryHeight: 200});
      dismissKeyboard();
    }
    else
      this.setState({accesoryHeight: 0});
  }

  hideAccesory = () => {
    if(this.state.accesoryHeight>0) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
      this.setState({accesoryHeight: 0});
    }
  }

  renderActions = () => {
    if(this.state.keyboardOpen) {
      hideAccesory();
    }
    return (
      <TouchableOpacity
        onPress={this.handleAddAction}
        underlayColor={'#444444'}
        style={{width:40, height:40, alignItems: 'center', paddingTop:5}}>
        <Icon
          name='ios-add'
          size={26}
          color={Colors.blackColor}
          />
      </TouchableOpacity>
    )
  }
  
  setChatText = (text) => {
    this.setState({text:text})
  }

  render() {
    var bottomStyle = {height:this.state.accesoryHeight};
    return (
      <View style={{flex:1}}>
        <GiftedChat
          keyboardShouldPersistTaps={'handled'}
          messages={this.state.messages}
          alwaysShowSend
          onLoadEarlier={this.onLoadEarlier}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
          renderMessage={this.renderMessage}
          onPressAvatar={this.handleAvatarPress}
          onPressActionButton={this.handleAddAction}
          renderActions={this.renderActions}
          onInputTextChanged={(text) => this.setChatText(text)}
        />

        <View style={{height: this.state.accesoryHeight, justifyContent: 'center', alignItems: 'center', overflow: 'hidden'}} removeClippedSubviews={true}>
          <View style={styles.row}>
            <View style={styles.columm}>
            <Text>A1</Text>
            </View>
            <View style={styles.columm}>
            <Text>A2</Text>
            </View>
            <View style={styles.columm}>
            <Text>A3</Text>
            </View>
            <View style={styles.columm}>
            <Text>A4</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.columm}>
            <Text>A5</Text>
            </View>
            <View style={styles.columm}>
            <Text>A6</Text>
            </View>
            <View style={styles.columm}>
            <Text>A7</Text>
            </View>
            <View style={styles.columm}>
            <Text>A8</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  accesory: {
    paddingTop: 0,
    justifyContent: 'center', 
    alignItems: 'center', 
    overflow: 'hidden',
    backgroundColor: '#ddd',
  },
  row: {
    height:80,
    flexDirection: 'row',
    alignItems: 'center',
  },
  columm: {
    flex: 1,
    alignItems: 'center',
  },
});
