import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import UserStore from '../store/UserStore';
import TestStore from '../store/TestStore';
import IconButton from '../components/IconButton';

export default class ProfileScreen extends React.Component {
  static navigationOptions = ({navigation, screenProps})=>({
    title: 'Profile',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerRight: (
			<IconButton
        handlePressAction={navigation.getParam('test')}
        name='ios-add'
        size={26}
        width={40}
        height={40}
        color={Colors.whiteColor}/>
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      uid:this.props.navigation.getParam('uid', ''),
    };
  }
  componentDidMount(){
    this.props.navigation.setParams({ test: this.test });
  }

  test =  (key) => {
    console.log('test');
    TestStore.connectHttp2();
  };
  
  render() {
    return (
      <View style={styles.container}>
        <Text>ProfileScreen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
