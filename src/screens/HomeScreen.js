import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  FlatList,
  View} from 'react-native';
import ActionSheet from 'react-native-actionsheet'
import { observer } from 'mobx-react';
import Mobx from 'mobx';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import UserStore from '../store/UserStore';
import BuddyListRow from '../components/BuddyListRow';
import IconButton from '../components/IconButton';

var ACTIONSEET_BUTTONS = [ "Add to Buddy", "Cancel"];

const sampleBuddy = [
  ['test1', '강철검', '테스트입니다.', 20, 'M'],
  ['test2', '신애라', 'ㅋㅋㅋ', 21, 'F'],
  ['test3', '고지용', '지금 만나요', 22, 'M'],
  ['test4', '박봄', '성남', 23, 'F'],
]
@observer
export default class HomeScreen extends React.Component {
  static navigationOptions =  ({navigation, screenProps})=>({
    title: 'Home',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerRight: (
			<IconButton
        handlePressAction={navigation.getParam('addUser')}
        name='ios-add'
        size={26}
        width={40}
        height={40}
        color={Colors.whiteColor}/>
    ),
  });

  constructor(props) {
    super(props);
    this.state = { 
      clicked:false,
      selectedItem:null,
    };
  }
  componentDidMount(){
    UserStore.loadBuddy();
    this.props.navigation.setParams({ addUser: this.addUser });
  }

  addUser =  (key) => {
    console.log('addUser');
    const index = UserStore.buddys.length;
    UserStore.createNewUser(sampleBuddy[index][0]
      , sampleBuddy[index][1]
      ,sampleBuddy[index][2]
      ,sampleBuddy[index][3]
      ,sampleBuddy[index][4]);
  };

  onPressItem =  (item) => {
    this.props.navigation.push('Profile', {uid:item.uid});
  };

  onLongPressItem =  (item) => {
    this.setState({
      selectedItem:item
    });
    this.ActionSheet.show();
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: Colors.seperator,
        }}
      />
    );
  }

  renderItem = ({item}) => (
    <BuddyListRow
      item={item}
      onPress={() => this.onPressItem(item)}
      onLongPress={() => this.onLongPressItem(item)}/>
  );
  handleActionSheet = (index) => {
    if(index ==0) {
      UserStore.addNewBuddy(this.state.selectedItem);
    }
    else if(index ==1) {

    }
    this.setState({
      selectedItem:null
    });
  };

  render() {
    const dataSource = UserStore.userInfos.slice();
    return (
      <View style={styles.container}>
        <FlatList
          data={dataSource}
          keyExtractor={(item, index) => item.uid}
          ItemSeparatorComponent = {this.FlatListItemSeparator}
          renderItem={this.renderItem}
        />
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={ACTIONSEET_BUTTONS}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={this.handleActionSheet}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
