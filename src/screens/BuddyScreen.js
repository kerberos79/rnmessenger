import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  FlatList,
  View} from 'react-native';
import ActionSheet from 'react-native-actionsheet'
import { observer } from 'mobx-react';
import Mobx from 'mobx';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import UserStore from '../store/UserStore';
import BuddyListRow from '../components/BuddyListRow';
import IconButton from '../components/IconButton';

var ACTIONSEET_BUTTONS = [ "Delete", "Cancel"];

@observer
export default class BuddyScreen extends React.Component {
  static navigationOptions =  ({navigation, screenProps})=>({
    title: 'Buddy',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  });

  constructor(props) {
    super(props);
    this.state = { 
      clicked:false,
      selectedItem:null,
    };
  }
  componentDidMount(){
    UserStore.loadBuddy();
  }

  onPressItem =  (item) => {
    this.props.navigation.push('Profile', {uid:item.uid});
  };

  onLongPressItem =  (item) => {
    this.setState({
      selectedItem:item
    });
    this.ActionSheet.show();
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: Colors.seperator,
        }}
      />
    );
  }

  renderItem = ({item}) => (
    <BuddyListRow
      item={item}
      onPress={() => this.onPressItem(item)}
      onLongPress={() => this.onLongPressItem(item)}/>
  );
  handleActionSheet = (index) => {
    if(index ==0) {
      
    }
    else if(index ==1) {

    }
    this.setState({
      selectedItem:null
    });
  };

  render() {
    const dataSource = UserStore.buddys.slice();
    return (
      <View style={styles.container}>
        <FlatList
          data={dataSource}
          keyExtractor={(item, index) => item.uid}
          ItemSeparatorComponent = {this.FlatListItemSeparator}
          renderItem={this.renderItem}
        />
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={ACTIONSEET_BUTTONS}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={this.handleActionSheet}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
