import React from 'react';
import {TouchableOpacity, 
  StyleSheet, 
  ScrollView, 
  TextInput, 
  Text,
  Alert,
  Image,
  View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';

export default class MoreScreen extends React.Component {
  static navigationOptions = {
    title: 'More',
    headerStyle: {
      backgroundColor: Colors.naviBackground,
    },
    headerTintColor: Colors.naviTitle,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>MoreScreen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
