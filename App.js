/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {AppState, Platform, StyleSheet, StatusBar, View} from 'react-native';
import AppNavigator from './src/navigation/AppNavigator';
import ChatStore from './src/store/ChatStore';
import UserStore from './src/store/UserStore';

type Props = {};
export default class App extends Component<Props> {

  state = {
    appState: AppState.currentState
  }

  componentDidMount() {
    UserStore.loadUserInfo();
    UserStore.loadBuddy();
    ChatStore.loadRooms();
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
    }
    this.setState({appState: nextAppState});

    console.log('##current App state : '+this.state.appState);
  }

  render() {
    return (
      <View style={styles.container}>
      {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
      <AppNavigator />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,    
    backgroundColor: '#ffffff',
  },
});
